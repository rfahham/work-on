FROM ubuntu:latest
LABEL maintainer="ricardo.fahham"
RUN apt-get update && apt-get upgrade -y
RUN apt-get install nginx -y
COPY workers.png index.html /usr/share/nginx/html/
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
